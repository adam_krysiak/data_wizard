
/*
 *  Klasa przechowująca podstawowe dane personalne
 *
 *  Autor: Adam Krysiak
 *   Data: 20.03.2016r
 *  
 */

import java.util.zip.DataFormatException;

/*
* @author Adam Krysiak
*
*/
public class Data {
	private String town;
	private String street;
	private Integer streetNumber;
	private Integer apartmentNumber;
	private String name;
	private String surname;
	private Integer phoneNumber;

	public String getName() {
		return name;
	}

	public void setName(String name) throws DataFormatException {
		if (!name.matches("[a-zA-Z]+"))
			throw new DataFormatException("BŁĄD IMIE MOŻE ZAWIERAĆ TYLKO LITERY ALFABETU ANGELSKIEGO");
		name = name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) throws DataFormatException {
		if (!surname.matches("[a-zA-Z]+"))
			throw new DataFormatException("BŁĄD NAZWISKO MOŻE ZAWIERAĆ TYLKO LITERY ALFABETU ANGELSKIEGO");
		surname = surname.substring(0, 1).toUpperCase() + surname.substring(1).toLowerCase();
		this.surname = surname;
	}

	public String getPhoneNumber() {
		if (phoneNumber != null)
			return phoneNumber.toString();
		return "";
	}

	public void setPhoneNumber(Integer phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getTown() {
		return town;
	}

	public void setTown(String town) throws DataFormatException {
		if (!town.matches("[a-zA-Z ]+"))
			throw new DataFormatException("BŁĄD NAZWA MIASTA MOŻE ZAWIERAĆ TYLKO LITERY ALFABETU ANGELSKIEGO");
		town = town.substring(0, 1).toUpperCase() + town.substring(1);
		this.town = town;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) throws DataFormatException {
		if (!street.matches("[a-zA-Z\t+[ 0-9+]]+"))
			throw new DataFormatException("BŁĄÐ NAZWA ULICY ZAWIERA ZNAKI SPECJALNE LUB SPOZA ALFABETU ANGELSKIEGO");
		street = street.substring(0, 1).toUpperCase() + street.substring(1);
		this.street = street;
	}

	public String getStreetNumber() {
		if (streetNumber != null)
			return streetNumber.toString();
		return "";
	}

	public void setStreetNumber(Integer streetNumber) {
		this.streetNumber = streetNumber;
	}

	public String getApartmentNumber() {
		if (apartmentNumber != null)
			return apartmentNumber.toString();
		return "";
	}

	public void setApartmentNumber(Integer apartmentNumber) {
		this.apartmentNumber = apartmentNumber;
	}

}
