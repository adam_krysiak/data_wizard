
/*
 *  Program umożliwiający wprowadzenie oraz przeglądanie podstawowych danych personalnych
 *
 *  Autor: Adam Krysiak
 *   Data: 20.03.2016r
 */

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Vector;
import java.util.zip.DataFormatException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * @author Adam Krysiak
 *
 */
public class MainControl extends JFrame implements ActionListener, KeyListener {

	private static final long serialVersionUID = 1L;
	JButton buttonNext = new JButton("dalej");
	JButton buttonPrev = new JButton("powrót");
	Vector<JTextField> textFields = new Vector<JTextField>();
	Vector<JLabel> labelsBeforeTextFields = new Vector<JLabel>();
	Vector<JLabel> labelsBeforeFinalDataAreas = new Vector<JLabel>();
	Vector<JLabel> labelsWithFinalData = new Vector<JLabel>();

	final int HOW_MANY_OPERATIONS = 5;
	final int HOW_MANY_DATA_FIELDS = 4;
	int operationCounter = 0;
	Data personalData = new Data();
	private JPanel window;


	MainControl() {
		super();
		this.setTitle("Data Wizard");
		this.setSize(new Dimension(400, 220));
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setResizable(false);
		
		window = new JPanel();
		window.setLayout(null);
		
		buttonNext.addKeyListener(this);
		buttonPrev.addActionListener(this);
		window.add(buttonNext);
		window.add(buttonPrev);
		
		window.addKeyListener(this);
		window.setFocusable(true);
		
		buttonNext.setBounds(299, 145, 69, 25);
		buttonPrev.setBounds(202, 145, 85, 25);

		this.buttonNext.addActionListener(this);
		this.buttonPrev.addActionListener(this);
		
		for (int i = 0; i < HOW_MANY_DATA_FIELDS; i++) {
			JTextField area = new JTextField();
			this.textFields.add(area);
			this.labelsBeforeTextFields.addElement(new JLabel());
			labelsBeforeTextFields.get(i).setBounds(20, 20 + 30 * i, 115, 20);
			textFields.get(i).setBounds(150, 20 + 30 * i, 140, 20);
			textFields.get(i).addKeyListener(this);
			window.add(labelsBeforeTextFields.get(i));
			window.add(textFields.get(i));
		}

		SetStartingPoint();

		this.setContentPane(window);
		JLabel lblFInstrukcja = new JLabel("F1 - instrukcja");
		lblFInstrukcja.setFont(new Font("Dialog", Font.PLAIN, 9));
		lblFInstrukcja.setBounds(22, 152, 74, 15);
		
		window.add(lblFInstrukcja);

		for (int i = 0; i < HOW_MANY_DATA_FIELDS; i++) {
			JLabel temp = new JLabel();
			temp.setVisible(false);
			temp.setBounds(130, 38 + 20 * i, 250, 15);
			labelsWithFinalData.add(temp);
			window.add(temp);
		}

		labelsBeforeFinalDataAreas.add(new JLabel("Imie:"));
		labelsBeforeFinalDataAreas.add(new JLabel("Nazwisko:"));
		labelsBeforeFinalDataAreas.add(new JLabel("Adres:"));
		labelsBeforeFinalDataAreas.add(new JLabel("Telefon:"));

		int i = 0;
		for (JLabel label : labelsBeforeFinalDataAreas) {
			label.setBounds(33, 38 + 20 * i, 85, 15);
			label.setVisible(false);
			window.add(label);
			i++;
		}

		this.setVisible(true);
	}


	/*
	 * Sets the view of JPanel to first step which is setting name
	 * 
	 */
	void SetStartingPoint() {
		this.SetNameView();
		this.buttonPrev.setEnabled(false);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();

		if (source.equals(buttonNext)) {
			if (this.operationCounter < HOW_MANY_OPERATIONS - 1 && this.operationCounter >= 0) {
				try {
					this.GetDataFromFields();
				} catch (NumberFormatException a) {
					JOptionPane.showMessageDialog(this, "BŁĘDNY FORMAT");
					return;
				} catch (DataFormatException a) {
					JOptionPane.showMessageDialog(this, a.getMessage());
					return;
				}
				this.operationCounter++;
			}
		}

		else if (source.equals(buttonPrev)) {
			if (this.operationCounter < HOW_MANY_OPERATIONS && this.operationCounter > 0) {
				this.operationCounter--;
			}
		}

		if (this.operationCounter == 0) {
			this.buttonPrev.setEnabled(false);
			this.SetNameView();
		} else if (this.operationCounter == 1) {
			this.buttonPrev.setEnabled(true);
			this.SetSurnameView();
		} else if (this.operationCounter == 2) {
			this.SetAdressView();
		} else if (this.operationCounter == 3) {
			this.HideDataView();
			this.SetNumberView();
		} else if (this.operationCounter == 4) {
			this.ShowDataView();
		}

		this.SetDataToFields();

	}

	/*
	 * Sets the labels and text areas to be visible or invisible
	 * 
	 * @param count of text areas to be visible, labels with description are set
	 * automatically
	 */
	void setVisibleElements(int howManyVisible) {
		if (howManyVisible > HOW_MANY_DATA_FIELDS || howManyVisible < 0)
			return;

		for (int i = 0; i < howManyVisible; i++) {
			labelsBeforeTextFields.get(i).setVisible(true);
			textFields.get(i).setVisible(true);
		}
		for (int i = howManyVisible; i < HOW_MANY_DATA_FIELDS; i++) {
			labelsBeforeTextFields.get(i).setVisible(false);
			textFields.get(i).setVisible(false);
		}

	}

	/*
	 * Sets the view of JPanel to enter adress
	 */
	void SetAdressView() {
		setVisibleElements(4);

		this.labelsBeforeTextFields.get(0).setText("miasto: ");
		this.labelsBeforeTextFields.get(1).setText("ulica: ");
		this.labelsBeforeTextFields.get(2).setText("nr. ulicy: ");
		this.labelsBeforeTextFields.get(3).setText("nr. mieszkania: ");

	}

	/*
	 * Sets the view of JPanel to enter name
	 */
	void SetNameView() {
		setVisibleElements(1);
		this.labelsBeforeTextFields.get(0).setText("imię: ");

	}

	/*
	 * Sets the view of JPanel to enter surname
	 */
	void SetSurnameView() {
		setVisibleElements(1);
		this.labelsBeforeTextFields.get(0).setText("nazwisko: ");
	}

	/*
	 * Sets the view of JPanel to enter phone number
	 */

	void SetNumberView() {
		setVisibleElements(1);
		this.labelsBeforeTextFields.get(0).setText("nr. telefonu: ");
	}

	/*
	 * Gets already saved data from personalData atributes and writes it in text
	 * areas
	 */
	void SetDataToFields() {

		if (this.operationCounter == 0)
			this.textFields.get(0).setText(this.personalData.getName());

		else if (this.operationCounter == 1)
			this.textFields.get(0).setText(this.personalData.getSurname());

		else if (this.operationCounter == 2) {
			this.textFields.get(0).setText(this.personalData.getTown());
			this.textFields.get(1).setText(this.personalData.getStreet());
			this.textFields.get(2).setText(this.personalData.getStreetNumber().toString());
			this.textFields.get(3).setText(this.personalData.getApartmentNumber().toString());
		}

		else if (this.operationCounter == 3)
			this.textFields.get(0).setText(this.personalData.getPhoneNumber().toString());
	}

	/*
	 * Reads data from available text areas and saves it properly in
	 * personalData atributes Checks if data has been writen and written
	 * properly
	 */
	void GetDataFromFields() throws DataFormatException {

		if (textFields.get(0).getText().equals(""))
			throw new DataFormatException("BŁĄÐ ZOSTAŁY PUSTE POLA DO UZUPEŁNIENIA");

		if (this.operationCounter == 0)
			this.personalData.setName(textFields.get(0).getText());

		else if (this.operationCounter == 1)
			this.personalData.setSurname(textFields.get(0).getText());

		else if (this.operationCounter == 2) {
			this.personalData.setTown(textFields.get(0).getText());
			if (textFields.get(1).getText().equals("") || textFields.get(2).getText().equals(""))
				throw new DataFormatException("BŁĄÐ ZOSTAŁY PUSTE POLA DO UZUPEŁNIENIA");
			this.personalData.setStreet(textFields.get(1).getText());
			this.personalData.setStreetNumber(Integer.parseInt(textFields.get(2).getText()));

			if (textFields.get(3).getText().equals(""))
				return;
			this.personalData.setApartmentNumber(Integer.parseInt(textFields.get(3).getText()));
		}

		else if (this.operationCounter == 3) {
			if (textFields.get(0).getText().length() < 9)
				throw new DataFormatException("BŁĄD PODANY NUMER JEST ZA KRÓTKI");
			if (textFields.get(0).getText().length() > 9)
				throw new DataFormatException("BŁĄD PODANY NUMER JEST ZA DŁUGI");
			
			this.personalData.setPhoneNumber(Integer.parseInt(textFields.get(0).getText()));
		}
	}

	/*
	 * Sets fields showing data visible and changes button buttonNext to be blocked
	 */

	void ShowDataView() {
		this.setVisibleElements(0);
		this.buttonNext.setEnabled(false);
		for (int i = 0; i < HOW_MANY_DATA_FIELDS; i++) {
			labelsBeforeFinalDataAreas.get(i).setVisible(true);
			labelsWithFinalData.get(i).setVisible(true);
		}
		this.labelsWithFinalData.get(0).setText(personalData.getName());
		this.labelsWithFinalData.get(1).setText(personalData.getSurname());

		String adressSTR = personalData.getTown() + ", ul. " + personalData.getStreet() + " "
				+ personalData.getStreetNumber();
		if (!personalData.getApartmentNumber().equals(""))
			adressSTR += "/" + personalData.getApartmentNumber();
		this.labelsWithFinalData.get(2).setText(adressSTR);
		this.labelsWithFinalData.get(3).setText(personalData.getPhoneNumber());

	}

	/*
	 * Hides fildes showing data and changes button buttonNext to be unlocked
	 */
	void HideDataView() {
		this.buttonNext.setEnabled(true);
		for (int i = 0; i < HOW_MANY_DATA_FIELDS; i++) {
			labelsBeforeFinalDataAreas.get(i).setVisible(false);
			labelsWithFinalData.get(i).setVisible(false);
		}

	}
	
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	
	@Override
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_ENTER:
			{
			this.buttonNext.doClick();
			break;
			}
		case KeyEvent.VK_ESCAPE:
		{
			this.buttonPrev.doClick();
			break;
		}
		case KeyEvent.VK_F1:{
			JOptionPane.showMessageDialog(this, "tab - następne pole \nshift+tab - poprzednie pole \nenter - dalej\nescape - powrót\nautor: Adam Krysiak");
		}
		}
		
	}
	
	
	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	
	/*
	 * Main function starting adventure
	 */
	public static void main(String[] args) {
		new MainControl();
	}
}
